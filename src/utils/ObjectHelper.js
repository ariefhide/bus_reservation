function isNullOrUndefined(val) {
  return val === null || val === undefined
}

function getValueByKey(obj, targets, valueIfNull = '') {
  if (isNullOrUndefined(obj)) {
    return valueIfNull
  }
  let targetSplit = targets.split('.')
  let value = null
  for (let target of targetSplit) {
    if (value) {
      value = value[target]
    }
    else {
      value = obj[target]
    }
    if (isNullOrUndefined(value)) {
      return valueIfNull
    }
  }
  return value
}

const ObjectHelpers = {
  getValueByKey
}

export default ObjectHelpers