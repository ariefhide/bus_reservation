import {
	SET_USER,
	SHOW_MODAL,
	TOGGLE_MODAL,
	SET_LOGIN,
	SET_KOTA,
	USER_LOGOUT,
	SET_START_DATE,
	SET_END_DATE,
	SET_ID_COMPANY,
	SET_BUS_TYPE,
	SET_BUS_ID
} from './types'

export const setStartDate = (from) => {
	return (dispatch) => {
		dispatch({
			type: SET_START_DATE,
			payload: {from}
		})
	}
}

export const setEndDate = (to) => {
	return (dispatch) => {
		dispatch({
			type: SET_END_DATE,
			payload: {to}
		})
	}
}

export const setBusID = (busID) => {
	return (dispatch) => {
		dispatch({
			type: SET_BUS_ID,
			payload: {busID}
		})
	}
}



export const setBusType = (bus) => {
	return (dispatch) => {
		dispatch({
			type: SET_BUS_TYPE,
			payload: {bus}
		})
	}
}

export const setIdCompany = (idCompany) => {
	return (dispatch) => {
		dispatch({
			type: SET_ID_COMPANY,
			payload: {idCompany}
		})
	}
}



export const logout = () => {
	
	return (dispatch) => {
		dispatch({
			type: USER_LOGOUT,
		})
	}
}


export const setUser = (user) => {
	return (dispatch) => {
		dispatch({
			type: SET_USER,
			payload: {user}
		})
	}
}

export const setKota = (kota) => {
	return (dispatch) => {
		dispatch({
			type: SET_KOTA,
			payload: {kota}
		})
	}
}

export const toggleModal = (name) => {
	return (dispatch) => {
		dispatch({
			type: TOGGLE_MODAL,
			payload: {name}
		})
	}
}

export const showModal = (name) => {
	return (dispatch) => {
		dispatch({
			type: SHOW_MODAL,
			payload: {name}
		})
	}
}

export const setLogin = () => {
	return (dispatch) => {
		dispatch({
			type: SET_LOGIN
		})
	}
}