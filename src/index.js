import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'font-awesome/css/font-awesome.min.css';
import * as serviceWorker from './serviceWorker';


//import reducer from './Redux/reducer';

import { createStore, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import thunk from "redux-thunk";
import reducers from './redux/reducers/AppReducer';

let store = createStore(reducers, applyMiddleware(thunk))


//const store = createStore(reducer);

ReactDOM.render(
	<Provider store={store}>
	<App/>
	</Provider>
	
	,		  
	document.getElementById('root')
	);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
