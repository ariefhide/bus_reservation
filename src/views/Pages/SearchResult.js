import React, { Component } from 'react';
import { PUBLIC_URL } from "../../constants";
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { setKota, setStartDate, setEndDate, setBusType, setIdCompany } from '../../redux/actions/AppAction';
import Axios from 'axios';
import '../../style/pages/searchResult.css';
/*
function searchingFor(idCompany) {
    return function (x) {
      return x.companyID.toString().toLowerCase().includes(idCompany.toString().toLowerCase()) || false;
    }
  }
  */
class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdown: false,
      locations: [],
      buss: [],
      idCompany: 1,
    };
  }
  componentDidMount() {
    Axios.get(`http://localhost:5000/company`)
      .then(res => {
        console.log(res, 'ini res company');
        this.setState({ locations: res.data });
      });
    Axios.get(`http://localhost:5000/bus`)
      .then(res => {
        console.log(res, 'ini res bus');
        this.setState({ buss: res.data });
      });
  }
  render() {
    const { kota, busType, idCompany } = this.props
    const { buss } = this.state
    return (
      <div>
        <div className="columns header">
          <div className="column is-6 dari">
            <h1>ONWARD JOURNEY</h1>
            <h3>Jakarta (Semua L...Yogyakarta (jogj...</h3>
          </div>
          <div className="column is-6 ke">
            <h1>RETURN JOURNEY</h1>
            <h3>Yogyakarta (Semua L... Jakarta (semua...</h3>
          </div>
        </div>
        <div className="columns ">
          <div className="column is-3 menu">
            <table className="table">
              <thead>
                <tr>
                  <h1>filter</h1>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <h1>Red Deals</h1>
                    <h1>JAM BERANGKAT</h1>
                    <ul>
                      <li><input type="checkbox" />Sebelum 6 am</li>
                      <li><input type="checkbox" />6 am to 12 pm</li>
                      <li><input type="checkbox" />12 pm to 6 pm</li>
                      <li><input type="checkbox" />Setelah 6 pm</li>
                    </ul>
                    <h1>Fasilitas</h1>
                    <span className="button is-danger is-outlined">Blankets</span>
                    <span className="button is-danger is-outlined">Snacks</span>
                    <span className="button is-danger is-outlined">Pillow</span>
                    <span className="button is-danger is-outlined">Safety belt </span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="column is-9">
            {
              buss.map(bus => (
                <div className="columns" >
                  <div className="column is-9 bus" key={bus.busID}>
                    <NavLink to="/BookingSummary">
                      <table className="table is-fullwidth">
                        <thead>
                          <tr>
                            <th>{bus.busName}</th>
                            <th>{bus.busSeat} seat</th>
                            <th>Rp {bus.busPrice} / 3 Day</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Executive (2+2)</td>
                            <td>Kalideres</td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                    </NavLink>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state
}
export default connect(mapStateToProps, { setKota, setStartDate, setEndDate, setBusType, setIdCompany } /*mapDispachToProps*/)(SearchResult);  