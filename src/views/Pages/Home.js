import React, { Component } from 'react';
import { PUBLIC_URL } from "../../constants";
import SideMenu from '../../components/SideMenu';
import Body from '../../components/Body';
import Date from '../../components/Date';
import { connect } from 'react-redux';
import { setStartDate, setEndDate } from '../../redux/actions/AppAction';
import '../../style/pages/home.css';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    return (
      <div >
        <img src={PUBLIC_URL + '/img/bus.jpg'} className="background" />
        <div className="columns">
          <div className="column is-6 is-offset-3">
            <h1 className="judul">Pusat Tiket bus Online Terbesar di Dunia</h1>
          </div>
        </div>
        <div className="columns" >
          <div className="column is-4 is-offset-2 date" style={{ marginTop: 10 }}>
            <Date />
          </div>
          <div className="column is-8 search">
            <SideMenu />
          </div>
        </div>
        <Body />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state
}
export default connect(mapStateToProps, { setStartDate, setEndDate } /*mapDispachToProps*/)(Home);