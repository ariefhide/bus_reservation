import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { PUBLIC_URL } from "../../constants";

import axios from 'axios';
import { connect } from 'react-redux';
import { setUser, showModal, toggleModal, setLogin } from '../../redux/actions/AppAction';

class BookingSummary extends Component {

  constructor(props) {
    super(props);

    this.state = {
      icon: "fa fa-eye-slash",
      password: "password",
      emailLogin: "",
      passwordLogin: "",
      bookings: [],

      apiUserID: '',
      apiUserEmail: '',
      apiUserName: '',
      apiUserPassword: '',
      apiUserPhone: '',

      namaRegist: "",
      emailRegist: "",
      passwordRegist: "",
      phoneRegist: "",
      namaRegistError: "*nama tidak boleh kosong",
      passwordRegistError: "",

      companyLocation: 'ggwp',
      startRent: 'fgg',
      endRent: 'ggg',
      busTypeName: 'ggg',
      fasilitas: 'gg',
      busPrice: 'gg',

    };
  }

  handlePassword = () => {
    if (this.state.password === "password") {
      this.setState({ password: "text", icon: "fa fa-eye" })
    } else if (this.state.password === "text") {
      this.setState({ password: "password", icon: "fa fa-eye-slash" })
    }
  }
  handleLogin = (e) => {
    this.props.showModal('modalLogin')
    //this.setState({ modalLogin: true });
  }
  handleSubmitLogin = (e) => {
    e.preventDefault();
    const data = {
      userEmail: e.target.elements.emailLogin.value,
      userPass: e.target.elements.passwordLogin.value,
    }

    //localStorage.setItem('data', JSON.stringify(data));

    //axios.get(`http://localhost:5000/users/${user}`)
    //.then((res) => {
    //console.log(res);
    //})
    if (data) {
      axios.post(`http://localhost:5000/login`, data)
        .then((res) => {
          console.log(res, "ress login")
          if (res.data.code === 204) {
            alert("email atau password salah")
            this.setState({ modalLogin: true })
          } else {
            alert("login berhasil")
            this.props.setUser(res.data.user)
            this.props.setLogin()
            this.setState({
              login: true,
              apiUserName: this.props.user.userName,
              apiUserID: this.props.user.userID,
              apiUserEmail: this.props.user.userEmail,
              apiUserPassword: this.props.user.userPass,
              apiUserPhone: this.props.user.userPhone
            })
            this.props.toggleModal('modalLogin')
          }
        })
    } else return;
  }
  closeModalLogin = () => {
    this.props.toggleModal('modalLogin')
  }
  handleEmailLogin = (e) => {
    e.preventDefault()
    this.setState({ emailLogin: e.target.value });
  }
  handlePasswordLogin = (e) => {
    e.preventDefault()
    this.setState({ passwordLogin: e.target.value });
  }
  handleRegister = (e) => {
    this.props.showModal('modalRegis')
    this.props.toggleModal('modalLogin')
    //this.setState({ modalRegis: true });
  }
  handleSubmitRegist = (e) => {
    e.preventDefault();
    const data = {
      userName: e.target.elements.namaRegist.value,
      userEmail: e.target.elements.emailRegist.value,
      userPass: e.target.elements.passwordRegist.value,
      userPhone: e.target.elements.phoneRegist.value,
    }
    //axios.get(`http://localhost:5000/users/${user}`)
    //.then((res) => {
    //console.log(res);
    //})
    if (data) {
      axios.post(`http://localhost:5000/users`, data)
        .then((res) => {
          console.log(res, "ress regist")
          if (res.data.message === "success") {
            alert("Registrasi berhasil silahkan Login")
            this.setState({ modalRegis: false })
          } else {
            alert("Email sudah ada")
          }
        })
    } else return;
  }
  handleNamaRegist = (e) => {
    this.setState({ namaRegist: e.target.value })
  }
  handlePasswordRegist = (e) => {
    this.setState({ passwordRegist: e.target.value })
  }
  handleEmailRegist = (e) => {
    this.setState({ emailRegist: e.target.value })
  }
  handlePhoneRegist = (e) => {
    this.setState({ phoneRegist: e.target.value })
  }
  closeModalRegist = () => {
    this.props.toggleModal('modalRegis')
  }
  componentDidMount() {

    axios.get(`http://localhost:5000/bookings/`)
      .then(res => {
        console.log(res, 'res get data booking');
        this.setState({ bookings: res.data });
      });
  }
  handleSubmitBooking = (e) => {
    e.preventDefault();
    const data = {
      companyLocation: e.target.elements.companyLocation.value,
      startRent: e.target.elements.startRent.value,
      endRent: e.target.elements.endRent.value,
      busTypeName: e.target.elements.busTypeName.value,
      fasilitas: e.target.elements.fasilitas.value,
      busPrice: e.target.elements.busPrice.value,
    }
    if (data) {
      axios.post(`http://localhost:5000/bookings`, data)
        .then((res) => {
          console.log(res, "ress booking baru")
          if (res.data.message === "success") {
            alert("nambah booking berhasil")
          } else {
            alert("booking sudah ada")
          }
        })
    } else return;
  }
  companyLocation = (e) => {
    this.setState({ companyLocation: e.target.value })
  }
  startRent = (e) => {
    this.setState({ startRent: e.target.value })
  }
  endRent = (e) => {
    this.setState({ endRent: e.target.value })
  }
  busTypeName = (e) => {
    this.setState({ busTypeName: e.target.value })
  }
  fasilitas = (e) => {
    this.setState({ fasilitas: e.target.value })
  }
  busPrice = (e) => {
    this.setState({ busPrice: e.target.value })
  }

  render() {
    const {
      emailLogin,
      password,
      passwordLogin,
      icon,
      namaRegist,
      namaRegistError,
      emailRegist,
      passwordRegist,
      passwordRegistError,
      phoneRegist
    } = this.state
    return (
      <div>
        <div className={'modal ' + (this.props.modalLogin && 'is-active')}>
          <div className="modal-background" onClick={() => this.closeModalLogin()}></div>
          <div className="modal-card">
            <header className="modal-card-head" style={{ backgroundColor: '#209cee' }}>
              <p className="modal-card-title" style={{ color: 'white' }}>Anda harus login terlebih dahulu</p>
              <button
                className="delete"
                aria-label="close"
                onClick={() => this.closeModalLogin()}
              />
            </header>
            <form onSubmit={(e) => { this.handleSubmitLogin(e) }} >
              <section className="modal-card-body">
                <div className="field">
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      placeholder="Email"
                      style={{ width: 350 }}
                      onChange={(e) => { this.handleEmailLogin(e) }}
                      value={emailLogin}
                      name="emailLogin"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <div className="control has-icons-left">
                    <input
                      className="input"
                      type={password}
                      placeholder="Password"
                      style={{ width: 350 }}
                      value={passwordLogin}
                      onChange={(e) => { this.handlePasswordLogin(e) }}
                      name="passwordLogin"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-lock" />
                    </span>
                    <a onClick={this.handlePassword} className="button" style={{ border: 'none', marginLeft: '10px' }}>
                      <span className="icon is-small is-right">
                        <i className={icon} />
                      </span>
                    </a>
                  </div>
                </div>
              </section>
              <footer className="modal-card-foot" style={{ backgroundColor: '#209cee' }}>
                <button
                  className="button is-success"
                  type="submit"
                >
                  LOGIN
                </button>
                <h1 style={{ color: 'white' }}>belum punya akun? <a onClick={(e) => { this.handleRegister(e) }} style={{ color: 'red' }}>register</a> disini</h1>
              </footer>
            </form>
          </div>
        </div>
        <div className={`modal ${this.props.modalRegis && 'is-active'}`}>
          <div className="modal-background" onClick={() => this.closeModalRegist()}></div>
          <div className="modal-card">
            <header className="modal-card-head" style={{ backgroundColor: '#209cee' }}>
              <p className="modal-card-title" style={{ color: 'white' }}>REGISTER</p>
              <button
                className="delete"
                aria-label="close"
                onClick={() => this.closeModalRegist()}
              />
            </header>
            <form onSubmit={(e) => { this.handleSubmitRegist(e) }}>
              <section className="modal-card-body">
                <div className="field">
                  <p>NAMA</p>
                  <div className="control ">
                    <input
                      className="input"
                      placeholder="Masukan nama"
                      style={{ width: 350 }}
                      value={namaRegist}
                      name="namaRegist"
                      onChange={(e) => this.handleNamaRegist(e)}
                    />

                    {
                      namaRegist === "" ?
                        <div style={{ color: 'red', fontSize: 12 }}>{namaRegistError}</div>
                        :
                        <div></div>
                    }
                  </div>
                </div>
                <div className="field">
                  <p>EMAIL</p>
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="email"
                      placeholder="Email"
                      style={{ width: 350 }}
                      value={emailRegist}
                      onChange={(e) => this.handleEmailRegist(e)}
                      name="emailRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>

                  </div>
                </div>
                <div className="field">
                  <p>PASSWORD</p>
                  <div className="control has-icons-left">
                    <input
                      className="input"
                      type={password}
                      placeholder="Password"
                      style={{ width: 350 }}
                      value={passwordRegist}
                      onChange={(e) => this.handlePasswordRegist(e)}
                      name="passwordRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-lock" />
                    </span>
                    <a onClick={this.handlePassword} className="button" style={{ border: 'none', marginLeft: '10px' }}>
                      <span className="icon is-small is-right">
                        <i className={icon} />
                      </span>
                    </a>
                    {
                      passwordRegist.length === 0 ?
                        <div style={{ color: 'red', fontSize: 12 }}>*password tidak boleh kosong</div>
                        :
                        <div style={{ color: 'red', fontSize: 12 }}>{passwordRegistError}</div>
                    }
                  </div>
                </div>
                <div className="field">
                  <p>Phone</p>
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="number"
                      placeholder="Phone"
                      style={{ width: 350 }}
                      value={phoneRegist}
                      onChange={(e) => this.handlePhoneRegist(e)}
                      name="phoneRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
              </section>
              <footer className="modal-card-foot" style={{ backgroundColor: '#209cee' }}>
                <button className="button is-success" >REGISTER</button>
              </footer>
            </form>
          </div>
        </div>

        <div style={{ marginTop: 100 }}>
          <table className="table is-hoverable">
            <thead>
              <tr>
                <th><abbr title="Position">Departure City</abbr></th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Bus Type</th>
                <th>Facility</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {this.state.bookings.map(booking =>
                <tr>
                  <th>
                    <span className="icon is-small is-left">
                      <i className="fa fa-bus" />
                    </span>
                    {booking.companyLocation}
                  </th>
                  <td>{booking.startRent}</td>
                  <td>{booking.endRent}</td>
                  <td>{booking.busTypeName}</td>
                  <td>{booking.fasilitas}</td>
                  <td>
                    <span className="icon is-small is-left">
                      <i className="fa fa-tag" />
                    </span>{booking.busPrice}
                  </td>
                </tr>
              )}
            </tbody>
          </table>
          {
            this.props.login ?
              <div className="content">
                <NavLink to="/BookingDetail"><a className="button is-success">SELECT</a></NavLink>
              </div>
              :
              <div className="content">
                <a className="button is-success" onClick={(e) => { this.handleLogin(e) }}>SELECT</a>
              </div>
          }
        </div>
      </div>

    );
  }
}
const mapStateToProps = (state) => {
  return state
}

export default connect(mapStateToProps, { setUser, showModal, toggleModal, setLogin } /*mapDispachToProps*/)(BookingSummary);  