import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import './App.css';

import Navbar from './components/Navbar';
import Footer from './components/Footer';

import Home from './views/Pages/Home';
import SearchResult from './views/Pages/SearchResult';
import BookingSummary from './views/Pages/BookingSummary';
import BookingDetail from './views/Pages/BookingDetail';
import Transaksi from './views/Pages/Transaksi';
import Error from "./views/Pages/Error";
import StatusBooking from './views/Pages/StatusBooking';

import { BrowserRouter, Route, Switch } from "react-router-dom";


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <div>
            <Navbar />
          </div>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/SearchResult" component={SearchResult} />
            <Route path="/BookingSummary" component={BookingSummary} />
            <Route path="/BookingDetail" component={BookingDetail} />
            <Route path="/Transaksi" component={Transaksi} />
            <Route path="/StatusBooking" component={StatusBooking} />
            <Route component={Error} />
          </Switch>
          <div>
            <Footer />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;