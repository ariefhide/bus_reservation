import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Departure from './Departure';
import Axios from 'axios';
import '../style/components/sideMenu.css';

import { connect } from 'react-redux';
import { setKota, setIdCompany, setBusType, setBusID } from '../redux/actions/AppAction';


function searchingFor(idCompany) {
  return function (x) {
    return x.companyID.toString().toLowerCase().includes(idCompany.toString().toLowerCase()) || false;
  }
}


class SideMenu extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dropdown: false,
      locations: [],
      kota: "",
      bus: 'Bus type',
      dropKota: false,
      busTypes: [],
      buss: [],
      idCompany: '',
      busID: '',
    };
  }
  dropdownProfile = () => {
    const { dropdown } = this.state
    if (dropdown === false) {
      this.setState({ dropdown: true });
    } else {
      this.setState({ dropdown: false });
    }
  }

  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }

  componentDidMount() {

    Axios.get(`http://localhost:5000/company`)
      .then(res => {
        console.log(res, 'ini res company');
        this.setState({ locations: res.data });
      });

    Axios.get(`http://localhost:5000/bus`)
      .then(res => {
        console.log(res, 'ini res bus');
        this.setState({ buss: res.data });
      });

    Axios.get(`http://localhost:5000/bus-type`)
      .then(res => {
        console.log(res, 'ini res bus type');
        this.setState({ busTypes: res.data });
      });

  }

  handleSearch = (e) => {
    const { dropKota } = this.state
    if (dropKota === false) {
      this.setState({ kota: e.target.value, dropKota: true });
    } else {
      this.setState({ kota: e.target.value, dropKota: false });
    }
  }

  ambilKota = (e, location) => {
    this.setState({ kota: location.companyLocation, idCompany: location.companyID, dropKota: false })
  }

  ambilBus = (e, busType) => {
    this.setState({ bus: busType.busTypeName, dropdown: false })
  }

  ambilBusID = (e, bus) => {
    this.setState({ busID: bus.busID })
  }

  handleSubmitKota = (e, bus) => {
    e.preventDefault();
    this.setState({ kota: this.state.kota })
    this.props.setKota(this.state.kota)
    this.props.setBusType(this.state.bus)
    this.props.setBusID(this.state.busID)
    this.props.setIdCompany(this.state.idcCompany)
  }

  render() {
    const {
      dropdown,
      busTypes,
      bus
    } = this.state
    return (
      <div>
        <div className="columns">
          <div className="column is-3">
            <div class="control has-icons-left has-icons-right" >
              <div className={`dropdown ${dropdown && 'is-active'} is-left busType`}>
                <div className="dropdown-trigger" >
                  <span className="icon is-large is-left" style={{ color: '#757575', fontSize: 22 }}>
                    <i class="fa fa-bus"></i>
                  </span>
                  <button
                    onClick={this.dropdownProfile}
                    className="input"
                    aria-haspopup="true"
                    aria-controls="dropdown-menu6"
                  >
                    <span style={{ color: '#757575' }}>{bus}</span>
                    <span className="icon is-medium is-right" style={{ color: '#757575' }}>
                      <i className="fa fa-angle-down" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
                <div className="dropdown-menu" id="dropdown-menu6" role="menu">
                  {
                    busTypes.map(busType => (
                      <div className="dropdown-content" key={busType.busTypeID} style={{ borderRadius: 0, width: 200, marginTop: -5 }}>
                        <a className="dropdown-item" onClick={(e) => { this.ambilBus(e, busType) }} value={bus} >
                          {busType.busTypeName}
                        </a>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
          <div className="column is-3">
            <Departure />
          </div>
          <div className="column is-2">
            <Link to='/SearchResult'><button className="button buttonSearch" type="submit" onClick={this.filter} >SEARCH</button></Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state
}

export default connect(mapStateToProps, { setKota, setIdCompany, setBusType, setBusID } /*mapDispachToProps*/)(SideMenu);  