import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import { NavLink } from 'react-router-dom';
import { PUBLIC_URL } from "../constants";
import '../style/components/navbar.css';
import axios from 'axios';

import { connect } from 'react-redux';
import { logout, setUser, showModal, toggleModal, setLogin } from '../redux/actions/AppAction';

class Navbar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      navbar: "navbar-menu",
      dropdown: "dropdown ",
      icon: "fa fa-eye-slash",
      password: "password",
      modalEdit: false,
      data: {},

      apiUserID: '',
      apiUserName: '',
      apiUserEmail: "",
      apiUserPassword: "",
      apiUserPhone: "",

      emailLogin: "",
      passwordLogin: "",
      namaRegist: "",
      emailRegist: "",
      passwordRegist: "",
      phoneRegist: "",
      namaRegistError: "*nama tidak boleh kosong",
      passwordRegistError: "",

      persons: [],
    };
  }

  handleNavbar = () => {
    if (this.state.navbar === "navbar-menu") {
      this.setState({ navbar: "navbar-menu is-active" })
    } else {
      this.setState({ navbar: "navbar-menu" })
    }
  }
  dropdownProfile = () => {
    if (this.state.dropdown === "dropdown is-right") {
      this.setState({ dropdown: "dropdown is-right is-active" })
    } else {
      this.setState({ dropdown: "dropdown is-right" })
    }
  }
  handleEdit = (e) => {
    this.setState({ modalEdit: true });
  }
  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }
  handlePassword = () => {
    if (this.state.password === "password") {
      this.setState({ password: "text", icon: "fa fa-eye" })
    } else if (this.state.password === "text") {
      this.setState({ password: "password", icon: "fa fa-eye-slash" })
    }
  }
  handleLogin = (e) => {
    this.props.showModal('modalLogin')
  }
  handleRegister = (e) => {
    this.props.showModal('modalRegis')
  }
  handleAkun = (item) => {
    this.setState({ [item]: !this.state[item] });
  }
  logout = () => {
    this.setState({ emailLogin: "", passwordLogin: "", })
    this.props.logout()
    alert("Terima kasih")
  }
  handleSubmitLogin = (e) => {
    e.preventDefault();

    const data = {
      userEmail: e.target.elements.emailLogin.value,
      userPass: e.target.elements.passwordLogin.value,
    }

    if (data) {
      axios.post(`http://localhost:5000/login`, data)
        .then((res) => {
          console.log(res, "ress login")
          if (res.data.code === 204) {
            alert("email atau password salah")
            this.setState({ modalLogin: true })
          } else {
            alert("login berhasil")
            this.props.setUser(res.data.user)
            this.props.setLogin()
            this.setState({
              login: true,
              apiUserName: this.props.user.userName,
              apiUserID: this.props.user.userID,
              apiUserEmail: this.props.user.userEmail,
              apiUserPassword: this.props.user.userPass,
              apiUserPhone: this.props.user.userPhone

            })
            this.props.toggleModal('modalLogin')
          }
        })
    } else return;
  }

  closeModalLogin = () => {
    this.props.toggleModal('modalLogin')
  }
  closeModalRegist = () => {
    this.props.toggleModal('modalRegis')
  }
  componentDidMount() {

    axios.get(`http://localhost:5000/users/`)
      .then(res => {
        console.log(res, 'res get data');
        this.setState({ persons: res.data });
      });
  }
  handleEmailLogin = (e) => {
    e.preventDefault()
    this.setState({ emailLogin: e.target.value });
  }
  handlePasswordLogin = (e) => {
    e.preventDefault()
    this.setState({ passwordLogin: e.target.value });
  }
  handleSubmitRegist = (e) => {
    e.preventDefault();

    const data = {
      userName: e.target.elements.namaRegist.value,
      userEmail: e.target.elements.emailRegist.value,
      userPass: e.target.elements.passwordRegist.value,
      userPhone: e.target.elements.phoneRegist.value,
    }

    if (data) {
      axios.post(`http://localhost:5000/users`, data)
        .then((res) => {
          console.log(res, "ress regist")
          if (res.data.message === "success") {
            alert("Registrasi berhasil silahkan Login")
            this.setState({ modalRegis: false })
          } else {
            alert("Email sudah ada")
          }
        })
    } else return;
  }
  handleNamaRegist = (e) => {
    this.setState({ namaRegist: e.target.value })
  }
  handlePasswordRegist = (e) => {
    this.setState({ passwordRegist: e.target.value })
  }
  handleEmailRegist = (e) => {
    this.setState({ emailRegist: e.target.value })
  }
  handlePhoneRegist = (e) => {
    this.setState({ phoneRegist: e.target.value })
  }
  handleSubmitEdit = (e) => {
    e.preventDefault();

    const data = {
      userName: e.target.elements.namaEdit.value,
      userEmail: e.target.elements.emailEdit.value,
      userPass: e.target.elements.passwordEdit.value,
      userPhone: e.target.elements.phoneEdit.value,
      userID: this.state.apiUserID,
    }

    if (data) {
      axios.put(`http://localhost:5000/users/${data.userID}`, data)
        .then((res) => {
          console.log(res, "ress")
          if (res.data.message === "success") {
            alert("Profil berhasil di edit")
            this.setState({ modalEdit: false })
          } else {
            alert("Email sudah ada")
          }
        })
    } else return;
  }
  handleNamaEdit = (e) => {
    this.setState({ apiUserName: e.target.value })
  }
  handlePasswordEdit = (e) => {
    this.setState({ apiUserPass: e.target.value })
  }
  handleEmailEdit = (e) => {
    this.setState({ apiUserEmail: e.target.value })
  }
  handlePhoneEdit = (e) => {
    this.setState({ apiUserPhone: e.target.value })
  }

  render() {
    const {
      user,
      login,
      modalLogin,
      modalRegis,
    } = this.props

    const { 
      dropdown,
      navbar,
      modalEdit,
      password,
      icon,
      emailLogin,
      passwordLogin,
      namaRegist,
      emailRegist,
      passwordRegist,
      namaRegistError,
      passwordRegistError,
      phoneRegist,
      apiUserName,
      apiUserEmail,
      apiUserPassword,
      apiUserPhone,
    } = this.state

    return (
      <div>
        <div className={`modal ${modalLogin && 'is-active'}`}>
          <div className="modal-background" onClick={() => this.closeModalLogin()}></div>
          <div className="modal-card login">
            <header className="modal-card-head ">
              <img src={PUBLIC_URL + '/img/logo.png'} />
              <p className="modal-card-title">LOGIN AKUN</p>
              <button
                className="delete"
                aria-label="close"
                onClick={() => this.closeModalLogin()}
              />
            </header>
            <form onSubmit={(e) => { this.handleSubmitLogin(e) }} >
              <section className="modal-card-body">
                <div className="columns">
                  <div className="column is-5">
                    <img src={PUBLIC_URL + '/img/login.jpg'} style={{height:200}}/>
                  </div>
                  <div className="column is-7">
                    <div className="field">
                      <div className="control has-icons-left has-icons-right">
                        <input
                          className="input"
                          placeholder="Email"
                          style={{ width: 350 }}
                          onChange={(e) => { this.handleEmailLogin(e) }}
                          value={emailLogin}
                          name="emailLogin"
                        />
                        <span className="icon is-small is-left">
                          <i className="fa fa-envelope" />
                        </span>
                        <span className="icon is-small is-right">
                          <i className="fa fa-check" />
                        </span>
                      </div>
                    </div>
                    <div className="field">
                      <div className="control has-icons-left has-icons-right">
                        <input
                          className="input"
                          type={password}
                          placeholder="Password"
                          style={{ width: 350 }}
                          value={passwordLogin}
                          onChange={(e) => { this.handlePasswordLogin(e) }}
                          name="passwordLogin"
                        />
                        <span className="icon is-small is-left">
                          <i className="fa fa-lock" />
                        </span>
                          <span className="icon is-small is-right" onClick={this.handlePassword}>
                              <i className={icon}  style={{ border: 'none', marginLeft: '10px'}}/>
                          </span>
                      </div>
                    </div>
                    <button
                      className="button"
                      onClick={() => this.handleAkun('modalLogin')}
                      type="submit"
                    >
                      LOGIN
                    </button>
                  </div>
                </div>
              </section>
            </form>
          </div>
        </div>

        <div className={`modal ${modalRegis && 'is-active'}`}>
          <div className="modal-background" onClick={() => this.closeModalRegist()}></div>
          <div className="modal-card registrasi">
            <header className="modal-card-head">
              <img src={PUBLIC_URL + '/img/logo.png'} />
              <p className="modal-card-title">REGISTER</p>
              <button
                className="delete"
                aria-label="close"
                onClick={() => this.closeModalRegist()}
              />
            </header>
            <form onSubmit={(e) => { this.handleSubmitRegist(e) }}>
              <section className="modal-card-body">
                <div className="field">
                  <p>NAMA</p>
                  <div className="control ">
                    <input
                      className="input"
                      placeholder="Masukan nama"
                      style={{ width: 350 }}
                      value={namaRegist}
                      name="namaRegist"
                      onChange={(e) => this.handleNamaRegist(e)}
                    />

                    {
                      namaRegist === "" ?
                        <div style={{ color: 'red', fontSize: 12 }}>{namaRegistError}</div>
                        :
                        <div></div>
                    }
                  </div>
                </div>
                <div className="field">
                  <p>EMAIL</p>
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="email"
                      placeholder="Email"
                      style={{ width: 350 }}
                      value={emailRegist}
                      onChange={(e) => this.handleEmailRegist(e)}
                      name="emailRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <p>PASSWORD</p>
                  <div className="control has-icons-left">
                    <input
                      className="input"
                      type={password}
                      placeholder="Password"
                      style={{ width: 350 }}
                      value={passwordRegist}
                      onChange={(e) => this.handlePasswordRegist(e)}
                      name="passwordRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-lock" />
                    </span>
                    <a onClick={this.handlePassword} className="button" style={{ border: 'none', marginLeft: '10px' }}>
                      <span className="icon is-small is-right">
                        <i className={icon} />
                      </span>
                    </a>
                    {
                      passwordRegist.length === 0 ?
                        <div style={{ color: 'red', fontSize: 12 }}>*password tidak boleh kosong</div>
                        :
                        <div style={{ color: 'red', fontSize: 12 }}>{passwordRegistError}</div>
                    }
                  </div>
                </div>
                <div className="field">
                  <p>Phone</p>
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="number"
                      placeholder="Phone"
                      style={{ width: 350 }}
                      value={phoneRegist}
                      onChange={(e) => this.handlePhoneRegist(e)}
                      name="phoneRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <button className="button" >REGISTER</button>
              </section>
            </form>
          </div>
        </div>

        <div className={`modal ${modalEdit && 'is-active'}`}>
          <div className="modal-background" onClick={() => this.toggle('modalEdit')}></div>
          <div className="modal-card">
            <header className="modal-card-head" style={{ baground: '#209cee', color: 'white' }}>
              <p className="modal-card-title">EDIT PROFILE</p>
              <button
                className="delete"
                aria-label="close"
                onClick={() => this.toggle('modalEdit')}
              />
            </header>
            <form onSubmit={(e) => { this.handleSubmitEdit(e) }}>
              <section className="modal-card-body" >
                <div className="field">
                  <p>Nama</p>
                  <div className="control ">
                    <input
                      className="input"
                      type="text"
                      name="namaEdit"
                      value={apiUserName}
                      placeholder='userName'
                      style={{ width: 350 }}
                      onChange={(e) => { this.handleNamaEdit(e) }}
                    />
                  </div>
                </div>
                <div className="field">
                  <p>Email</p>
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="email"
                      name="emailEdit"
                      value={apiUserEmail}
                      placeholder="masukan email baru"
                      style={{ width: 350 }}
                      onChange={(e) => { this.handleEmailEdit(e) }}
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <p>Password</p>
                  <div className="control has-icons-left">
                    <input
                      className="input"
                      type={password}
                      name="passwordEdit"
                      value={apiUserPassword}
                      placeholder="masukan password baru"
                      style={{ width: 350 }}
                      onChange={(e) => { this.handlePasswordEdit(e) }}
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-lock" />
                    </span>
                    <a onClick={this.handlePassword} className="button" style={{ border: 'none', marginLeft: '10px' }}>
                      <span className="icon is-small is-right">
                        <i className={icon} />
                      </span>
                    </a>
                  </div>
                </div>
                <div className="field">
                  <p>Phone</p>
                  <div className="control ">
                    <input
                      className="input"
                      type="textarea"
                      name="phoneEdit"
                      value={apiUserPhone}
                      style={{ width: 350 }}
                      placeholder="masukan nomor hp baru"
                      onChange={(e) => { this.handlePhoneEdit(e) }}
                    />
                  </div>
                </div>
                <button className="button is-success" >SAVE</button>
              </section>
            </form>
          </div>
        </div>

        <nav className="navbar is-fixed-top navHead" role="navigation" aria-label="main navigation" style={{ background: '#d84f57' }}>
          <div className="navbar-brand" >
            <NavLink to="/">
                <img src={PUBLIC_URL + '/img/logo.png'} className="logo" />
            </NavLink>
            <a role="button" onClick={this.handleNavbar} className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
              <span style={{color:'white'}} aria-hidden="true"></span>
              <span style={{color:'white'}} aria-hidden="true"></span>
              <span style={{color:'white'}} aria-hidden="true"></span>
            </a>
          </div>
          <div id="navbarBasicExample" ref="toggle" className={navbar} >
            <div className="navbar-start navMenu" >
              <span className="navbar-item">Partner Kami</span>        
              <span className="navbar-item">Cara Membayar</span>
              <span className="navbar-item">Butuh Bantuan</span>
            </div>
            <div className="navbar-end navEnd">
                {
                  login ?
                    <div  style={{ color: 'white' }}>
                      <span style={{ marginRight: 10, fontSize: 20 }}> {user.userName}</span>
                      <div className={dropdown}>
                        <div className="dropdown-trigger">
                          <span className="icon is-large " onClick={this.dropdownProfile} aria-haspopup="true" aria-controls="dropdown-menu6" >
                            <img src={PUBLIC_URL + '/img/avatar.png'} className='logoAkun'/>
                            <i className="fa fa-angle-down" style={{fontSize:30,marginTop:10}}></i>
                          </span>
                        </div>
                        <div className="dropdown-menu" id="dropdown-menu6" role="menu">
                          <div className="dropdown-content dropdownProfile">
                            <div className="dropdown-item">
                              <a onClick={(e) => { this.handleEdit(e) }} className="dropdown-item">Edit Profile</a>
                            </div>
                            <div className="dropdown-item">
                              <NavLink to="/StatusBooking"><a className="dropdown-item">My Booking</a></NavLink>
                            </div>
                            <div className="dropdown-item">
                              <a className="dropdown-item">History</a>
                            </div>
                            <div className="dropdown-item">
                              <NavLink to="/"><a onClick={this.logout} className="dropdown-item">Logout</a></NavLink>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    :
                    <div style={{ width: 100, height: 50, color: 'white' }}>
                      <div className={dropdown}>
                        <div className="dropdown-trigger">
                          <span className="icon is-large " onClick={this.dropdownProfile}>
                            <img src={PUBLIC_URL + '/img/avatar.png'} className='logoAkun'/>
                            <i className="fa fa-angle-down" style={{fontSize:30,marginTop:10}}></i>
                          </span>
                        </div>
                        <div className="dropdown-menu" id="dropdown-menu6" role="menu">
                          <div className="dropdown-content dropdownProfile" >
                            <div className="dropdown-item" onClick={(e) => { this.handleLogin(e) }}>
                              <span >
                                LOGIN
                            </span>
                            </div>
                            <div className="dropdown-item" onClick={(e) => { this.handleRegister(e) }}>
                              <span >
                                REGISTER
                            </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                }
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state
}
export default connect(mapStateToProps, { logout, setUser, showModal, toggleModal, setLogin } /*mapDispachToProps*/)(Navbar);