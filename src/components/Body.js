import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import { PUBLIC_URL } from "../constants";
import '../style/components/body.css';
import ScrollAnimation from 'react-animate-on-scroll';
import "animate.css/animate.min.css";


class Body extends Component {
  render() {
    return (
      <div>
        <div className="sectionBody">
          <div className="columns">
            <div className="column is-5 is-offset-3 diskon">
              <div className="columns">
                <div className="column is-3">
                  <img src={PUBLIC_URL + '/img/diskon.jpg'} />
                </div>
                <div className="column is-7">
                  <h1>DAPATKAN RP.40.000 PER ORANG</h1>
                  <p>Maksimal bisa dapat hingga Rp.800.000</p>
                </div>
              </div>
            </div>
          </div>
          <div className="columns is-multiline">
            <div className="column is-3 is-offset-1 ">
              <img src={PUBLIC_URL + '/img/box1.jpg'} className="kartu" />
            </div>
            <div className="column is-3 ">
              <img src={PUBLIC_URL + '/img/box1.jpg'} className="kartu" />
            </div>
            <div className="column is-3 ">
              <img src={PUBLIC_URL + '/img/box1.jpg'} className="kartu" />
            </div>
          </div>
        </div>

        <div className="section2">
          <ScrollAnimation delay={1000} animateIn='bounceInLeft'>
            <h1 className="htiket">CARA PEMESANAN TIKET BIS</h1>
          </ScrollAnimation>
          <div className="columns">
            <div className="column is-3 is-offset-1 info">
              <ScrollAnimation delay={1200} animateIn="fadeIn">
                <img src={PUBLIC_URL + '/img/box2.jpg'} />
              </ScrollAnimation>
              <ScrollAnimation delay={1200} animateIn="fadeIn">
                <p className="ptext">
                  Pilih rincian perjalanan Masukkan tempat keberangkatan, tujuan,
                  tanggal perjalanan dan kemudian klik 'Cari'
              </p>
              </ScrollAnimation>
            </div>
            <div className="column is-3 info">
              <ScrollAnimation delay={1600} animateIn="fadeIn">
                <img src={PUBLIC_URL + '/img/box2.jpg'} />
              </ScrollAnimation>
              <ScrollAnimation delay={1600} animateIn="fadeIn">
                <p className="ptext">
                  Pilih bis dan tempat duduk anda Pilih bis, tempat duduk,
                  tempat keberangkatan, tujuan, isi rincian penumpang dan klik 'Pembayaran'
              </p>
              </ScrollAnimation>
            </div>
            <div className="column is-3 info">
              <ScrollAnimation delay={2000} animateIn="fadeIn">
                <img src={PUBLIC_URL + '/img/box2.jpg'} />
              </ScrollAnimation>
              <ScrollAnimation delay={2000} animateIn="fadeIn">
                <p className="ptext">
                  Cara Pembayaran yang Mudah Pembayaran dapat dilakukan melalui transfer ATM,
                  Internet banking, Alfamart, kartu Kredit/Debit, Mandiri Clickpay, Bca Clickpay dll
              </p>
              </ScrollAnimation>
            </div>
          </div>
        </div>
        <div>
          <img src={PUBLIC_URL + '/img/bromo.png'} className="backgroundBromo" />
          <ScrollAnimation delay={500} animateIn="fadeIn">
            <h1 className="h1Bromo">
              BEPERGIAN BERSAMA KAMI
               </h1>
            <p className="pBromo">
              redBus adalah platform pemesanan tiket bis online terbesar di dunia yang
              dipercaya oleh jutaan pelanggan secara global. redBus terdiri dari sekelompok perusahaan
              bis terbaik di Indonesia yang menawarkan pemesanan tiket yang paling cepat, mudah dan aman.
              Ini akan membantu anda dalam memilih destinasi, memilih tempat duduk yang diinginkan,
              dan memesan tiket bis hanya dalam beberapa klik. Dapatkan penawaran-penawaran menarik dari redBus
              dan susunlah sebuah perjalanan yang tak terlupakan!
          </p>
          </ScrollAnimation>
        </div>
      </div>
    )
  }
}
export default Body;