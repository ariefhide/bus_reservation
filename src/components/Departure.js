import React, { Component } from 'react';
import Axios from 'axios';
import '../style/components/sideMenu.css';

class Departure extends Component {
  constructor(props) {
    super(props);

    this.state = {
      locations: [],
      kota: "",
      dropKota: false,
      idCompany: '',

    };
  }
  componentDidMount() {

    Axios.get(`http://localhost:5000/company`)
      .then(res => {
        console.log(res, 'ini res company');
        this.setState({ locations: res.data });
      });

  }
  ambilKota = (e, location) => {
    this.setState({ kota: location.companyLocation, idCompany: location.companyID, dropKota: false })
  }
  handleSearch = (e) => {
    const { dropKota } = this.state
    if (dropKota === false) {
      this.setState({ kota: e.target.value, dropKota: true });
    } else {
      this.setState({ kota: e.target.value, dropKota: false });
    }
  }

  render() {
    const {
      kota,
      locations,
      dropKota
    } = this.state
    return (

      <div class="control has-icons-left has-icons-right departure" >
        <span class="icon is-large" style={{ color: '#757575', fontSize: 22 }}>
          <i class="fa fa-street-view is-left" ></i>
        </span>
        <input
          className="input"
          type="text"
          onChange={(e) => this.handleSearch(e)}
          value={kota}
          placeholder="Departure City"
        />
        <div className={`dropdown ${dropKota && 'is-active'}`}>
          <div className="dropdown-menu" id="dropdown-menu" role="menu" style={{ marginTop: 46, marginLeft: -200 }}>
            {
              locations.map(location => (
                <div key={location.companyID} className="dropdown-content" style={{ borderRadius: 0, width: 200, paddingLeft: 10 }} onClick={(e) => { this.ambilKota(e, location) }}>
                  <span >{location.companyLocation}</span>
                </div>
              ))}
          </div>
        </div>
      </div>
    );
  }
}
export default Departure;  