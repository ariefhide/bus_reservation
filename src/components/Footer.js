import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import '../style/components/footer.css';


class Footer extends Component {
  render() {
    return (
      <div>
        <section className="sectionFooter" >
          <div className="container" style={{ marginTop: -40 }}>
            <div className="columns " >
              <ul className="column is-2 footer">Tentang Redbus
                <li>Tentang Kami</li>
                <li>hubungi kami</li>
                <li>sitemap</li>
                <li>Versi ponsel</li>
              </ul>
              <ul className="column is-2 footer">Info
                  <li>T & C</li>
                <li>ketentuan privasi</li>
                <li>ketentuan cookie</li>
                <li>tanya jawab</li>
                <li>agent registrasion</li>
              </ul>
              <ul className="column is-2 footer">Situs global
                  <li>India</li>
                <li>Singapura</li>
                <li>Malaysia</li>
                <li>Indonesia</li>
                <li>Peru</li>
                <li>Colombia</li>
              </ul>
              <div className="column is-4 is-offset-2 footerKanan">
                <p>
                  redBus adalah jasa pemesanan tiket bis secara online terbesar di dunia.
                  Telah dipercaya lebih dari 8 juta pelanggan secara global.
                  redBus menawarkan pemesanan tiket bis melalui website,
                  iOS dan aplikasi android untuk rute-rute utama di Singapura, Malaysia, dan Indonesia.
                 </p>
                <br />
                <span className="icon sosmed">
                  <i className="fa fa-facebook"></i>
                </span>
                <span className="icon sosmed">
                  <i className="fa fa-twitter"></i>
                </span>
                <p>
                  Ⓒ 2018 ibibogroup All rights reserved
                </p>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default Footer;